package at.spengergasse.dbi;




import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class SchuelerTest {

    Schueler schueler = new Schueler("Bernhard", "Balik", LocalDate.of(2000, Month.MAY, 16), "4BHIF", 'm');


    @Test
    public void createSchueler() {
        assertTrue(schueler != null);
    }

    @Test
    public void setSchuelerName() {
        schueler.set_name("Benjamin");
        assertThat(schueler.get_name(), equalTo("Benjamin"));
    }

    @Test
    public void setSchuelerLastName() {
        schueler.set_lastname("Chihungi");
        assertThat(schueler.get_lastname(), equalTo("Chihungi"));
    }

    @Test
    public void setSchuelerDate() {
        schueler.set_dateofbirth(LocalDate.of(1999, Month.MARCH, 16));
        assertThat(schueler.get_dateofbirth(), equalTo(LocalDate.of(1999, Month.MARCH, 16)));
    }
}
