package at.spengergasse.dbi;

import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;


public class FachTest {

    Fach fach = new Fach();

    @Test
    public void createFach() {
        assertTrue(fach != null);
    }

    @Test
    public void setSubject() {
        fach.set_subject("Mathematik");
        assertThat(fach.get_subject(),equalTo("Mathematik"));
    }

    @Test
    public void setMark() {
        fach.set_mark(1);
        assertThat(fach.get_mark(), equalTo(1));
    }

}
