package at.spengergasse.dbi;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class ZeugnisTest {

    Schueler schueler = new Schueler("Bernhard", "Balik", LocalDate.of(2000, Month.MAY, 16), "4BHIF", 'm');
    Fach fach1 = new Fach("Mathematik",3);
    Fach fach2 = new Fach("Deutsch",3);
    Fach fach3 = new Fach("Englisch",3);
    Fach fach4 = new Fach("Religion",1);
    Fach fach5 = new Fach("Deutsch",3);

    ArrayList<Fach> _faecher = new ArrayList<Fach>(Arrays.asList(fach1,fach2,fach3,fach4,fach5));

    Zeugnis zeugnis = new Zeugnis();

    @Test
    public  void createZeugnis() {
        assertTrue(zeugnis != null);
    }

    @Test
    public void addFaecherWitherror() {
        //tried to add 5 subjects. One f them is a duplicate. so we expect 4 subjects to be added
        zeugnis.set_faecher(_faecher);
        assertThat(zeugnis.get_faecher().size(), equalTo(4));
    }

    @Test
    public void setSchueler() {
        zeugnis.set_schueler(schueler);
        assertThat(zeugnis.get_schueler(),equalTo(schueler));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setSchuelerWithError() {
        zeugnis.set_schueler(null);
    }
}