package at.spengergasse.dbi;


import java.time.LocalDate;

public class Schueler
{
    private String _name;
    private String _lastname;
    private LocalDate _dateofbirth;
    private String _class;
    private char _gender;


    public Schueler(String _name, String _lastname, LocalDate _dateofbirth, String _class, char _gender) {
        set_name(_name);
        set_lastname(_lastname);
        set_dateofbirth(_dateofbirth);
        set_class(_class);
        set_gender(_gender);
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_lastname() {
        return _lastname;
    }

    public void set_lastname(String _lastname) {
        this._lastname = _lastname;
    }

    public LocalDate get_dateofbirth() {
        return _dateofbirth;
    }

    public void set_dateofbirth(LocalDate _dateofbirth) {
        this._dateofbirth = _dateofbirth;
    }

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public char get_gender() {
        return _gender;
    }

    public void set_gender(char _gender) {
        this._gender = _gender;
    }
}
