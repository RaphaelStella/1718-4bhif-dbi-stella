package at.spengergasse.dbi;

import java.util.ArrayList;

public class Zeugnis {

    private  Schueler _schueler;
    private ArrayList<Fach> _faecher = new ArrayList<>();

    public Zeugnis() {

    }

    public Zeugnis(Schueler _schueler, ArrayList<Fach> _faecher) {
        set_schueler(_schueler);
        set_faecher(_faecher);
    }

    public Schueler get_schueler() {
        return _schueler;
    }

    public void set_schueler(Schueler _schueler) {
        try{
            if(_schueler == null)
            { throw new IllegalArgumentException("Kein Schueler uebergeben"); }
            else{this._schueler = _schueler;}
        }catch(Exception e)
        {
            System.out.print("Fehler!" + e.getMessage());
            throw  new IllegalArgumentException();
        }
    }

    public ArrayList<Fach> get_faecher() {
        return _faecher;
    }

    public void set_faecher(ArrayList<Fach> _faecher) {

        for (Fach f: _faecher) {
            addFach(f);
        }
    }

    public void addFach( Fach _fach){
        boolean isinthelist = false;
            for (Fach f: _faecher) {
                if (f.get_subject() == _fach.get_subject()) {
                    isinthelist = true;
                    break;
                } else {
                    isinthelist = false;
                }
            }

            try {
                if(isinthelist) {
                    throw  new IllegalArgumentException("Das Fach: " + _fach.get_subject() + " ist bereits in der Liste");
                }
                else {
                    _faecher.add(_fach);
                }
            }catch(Exception e)
            {
                System.out.print("Fehler!" + e.getMessage());
            }
    }
}
