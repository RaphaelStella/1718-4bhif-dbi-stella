package at.spengergasse.dbi;

public class Fach {
    private String _subject;
    private int _mark;

    public Fach()
    {

    }

    public Fach(String _subject, int _mark)
    {
        set_subject(_subject);
        set_mark(_mark);
    }

    public String get_subject() {
        return _subject;
    }

    public void set_subject(String _subject) {
        this._subject = _subject;
    }

    public int get_mark() {
        return _mark;
    }

    public void set_mark(int _mark) {
        this._mark = _mark;
    }
}
