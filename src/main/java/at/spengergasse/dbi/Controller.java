package at.spengergasse.dbi;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.ListCollectionsIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class Controller
{

    public Controller()
    {
       MongoClientURI mongoConnectionString = new MongoClientURI("mongodb://localhost:32768");
       MongoClient mongoClient = new MongoClient(mongoConnectionString);

        MongoDatabase database = mongoClient.getDatabase("Noten");
        ListCollectionsIterable<Document> collections = database.listCollections();
        MongoCollection<Document> collection = database.getCollection("");

        mongoClient.close();
    }

}
